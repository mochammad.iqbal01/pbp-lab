from django.db import models


# Create your models here.

class Note(models.Model):
    msg_to = models.CharField(max_length=30, default="")
    msg_from = models.CharField(max_length=30, default="")
    msg_title = models.CharField(max_length=30, default="")
    msg_message = models.TextField(default="")
