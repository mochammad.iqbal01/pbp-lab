Apakah perbedaan antara JSON dan XML?
XML (Extensible Markup Language):
- XML adalah bahasa markup, bukan bahasa pemrograman, yang memiliki tag untuk mendefinisikan elemen.
- Data XML disimpan sebagai tree structure.
- Dapat melakukan pemrosesan dan pemformatan dokumen dan objek.
- Besar dan lambat dalam penguraian, menyebabkan transmisi data lebih lambat
- Mendukung namespaces, komentar dan metadata
- Ukuran dokumen besar dan dengan file besar, struktur tag membuatnya besar dan rumit untuk dibaca.
- Tidak mendukung array secara langsung. Untuk dapat menggunakan sebuah array, seseorang harus menambahkan tag untuk setiap item.
- Mendukung banyak tipe data kompleks termasuk bagan, charts, dan tipe data non-primitif lainnya.
- XML mendukung pengkodean UTF-8 dan UTF-16.
- Struktur XML rentan terhadap beberapa serangan karena perluasan entitas eksternal dan validasi DTD diaktifkan secara default. Jika ini dinonaktifkan, pengurai XML lebih aman.
- Meskipun X di AJAX adalah singkatan dari XML, karena tag di XML, banyak bandwidth yang dikonsumsi secara tidak perlu, membuat permintaan AJAX menjadi lambat.

JSON (JavaScript Object Notation):
- JSON hanyalah format yang ditulis dalam JavaScript.
- Data disimpan seperti map dengan pasangan key value.
- Tidak melakukan pemrosesan atau perhitungan apa pun.
- Sangat cepat karena ukuran file sangat kecil, penguraian lebih cepat oleh mesin JavaScript dan karenanya transfer data lebih cepat.
- Tidak ada ketentuan untuk namespace, menambahkan komentar atau menulis metadata
- Ringkas dan mudah dibaca, tidak ada tag atau data yang tidak terpakai atau kosong, membuat file terlihat sederhana.
- Mendukung array yang dapat diakses.
- JSON hanya mendukung string, angka, array Boolean, dan objek. Bahkan objek hanya dapat berisi tipe primitif.
- JSON mendukung penyandiaksaraan UTF serta ASCII.
- Penguraian JSON aman hampir sepanjang waktu kecuali jika JSONP digunakan, yang dapat menyebabkan serangan Cross-Site Request Forgery (CSRF).
- Karena data diproses secara serial di JSON, menggunakannya dengan AJAX memastikan pemrosesan yang lebih cepat dan karenanya lebih disukai. Data dapat dengan mudah dimanipulasi menggunakan metode eval ().

Apakah perbedaan antara HTML dan XML?
XML (Extensible Markup Language):
- Menyediakan sebuah framework atau sebuah kerangka kerja untuk menentukan bahasa markup.
- Informasi pada XML disediakan.
- Case Sensitive atau peka terhadap besar dan kecil kalimat
- Bertujuan untuk transfer informasi.
- Tidak boleh ada error.
- Spasi bisa dipertahankan.
- Harus menggunakan tag penutup.
- Nesting atau urutan harus dilakukan dengan benar.

HTML (Hypertext Markup Language):
- HTML merupakan bahasa markup standar.
- Tidak mengandung informasi struktural.
- Case insesitive atau tidak peka terhadap besar dan kecil kalimat.
- Bertujuan untuk penyajian data.
- Error kecil dapat diabaikan.
- Tidak dapat mempertahankan spasi.
- Tag penutup bisa diberikan atau tidak (opsional).
- Nesting atau urutan tidak terlalu diperhitungkan.